#!/bin/sh

raspivid -o "$(date --iso-8601=s)".h264 -t 0 -pf high -lev 4.2 -if adaptive -g 25 -pts "$(date --iso-8601=s)".pts -fps 30
