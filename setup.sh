#!/bin/sh

sudo raspi-config # activate camera and autologin into shell
# raspivid -o vid.h264 -t 0 -pf high -lev 4.2 -if adaptive -g 25 -pts vid.pts -fps 30
# arecord -D hw:1,0 --format S16_LE --rate 48000 -c1 audio.wav

# pulse not really needed
#sudo apt-get install alsa-base pulseaudio
#sudo apt install pavucontrol
sudo apt install -y mkvtoolnix  # mkvmerge -o output.mkv --title 'Der Titel' --timestamps 0:vid.pts vid.h264
sudo apt install -y gpac        # MP4Box -add vid.h264 vid.mp4

# list devices and pick
arecord -l
#pacmd list-sources

#commands should be possible
#parecord --device="alsa_input.usb-0c76_USB_PnP_Audio_Device-00.analog-mono" audio.wav
#arecord --device="alsa_input.usb-0c76_USB_PnP_Audio_Device-00.analog-mono" audio.wav
#arecord -D hw:1,0 --format S16_LE --rate 48000 -c1 audio.wav

