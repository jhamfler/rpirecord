#!/bin/sh
[ -z "$1" ] && i="$1"

mkvmerge -o "$(date --iso-8601=s)".mkv --title "Wald $(date --iso-8601=s)" --timestamps 0:"$i".pts "$i".h264 "$i".wav
